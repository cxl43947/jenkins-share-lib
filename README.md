# jenkins-share-lib
#### 介绍
pipeline 扩展脚本\
所有流程直接在代码上控制 不依赖其他脚本\
代码简约,核心工具只有一个类\
目前支持构建: java项目,docker推送和下载,node项目,mysql备份与还原\
其他功能: 构建记录持久化(目前使用的ElasticSearch),钉钉通知,构建完成修改构建备注\
这个仓库供参考 方便二次开发 欢迎各位提交常用方法\
建议安装的插件 
1. Extended Choice Parameter    参数多选框
2. Generic Webhook Trigger       webhook
3. Pipeline         流水线插件 workflow
4. Blue Ocean           更直观查看pipeline构建过程
5. ...\
手动下载插件地址 [http://updates.jenkins-ci.org/download/plugins/](http://updates.jenkins-ci.org/download/plugins/)\
直接安装提前准备的[jenkins2.249.3](https://blog.csdn.net/chen_cxl/article/details/110119610)
#### 软件架构
- demo    示例部署脚本
- docker-compose  docker-compose模板
- resource        配置文件
- src 工具类位置   代码内部依赖宿主机maven和node
#### 使用说明
1. 添加jenkins=>全局配置=>Global Pipeline Libraries
 ![avatar](https://gitee.com/cxl43947/jenkins-share-lib/raw/master/1.png)
2.定义脚本  头部添加依赖
~~~
@Library('jenkins_util') _                     //引入共享库
def util=new com.util.Util()                     //共享库中的类
script{ //在script里使用
    util.xxx()
}
~~~ 
3.定义任务
 ![avatar](https://gitee.com/cxl43947/jenkins-share-lib/raw/master/2.png)
 第一次构建加载配置
 ![avatar](https://gitee.com/cxl43947/jenkins-share-lib/raw/master/3.png)
4.构建结果
 ![avatar](https://gitee.com/cxl43947/jenkins-share-lib/raw/master/4.png)
  钉钉通知
 ![avatar](https://gitee.com/cxl43947/jenkins-share-lib/raw/master/5.png)
#### 参考
##### maven-docker
~~~
 <plugin>
                <groupId>com.spotify</groupId>
                <artifactId>docker-maven-plugin</artifactId>
                <version>1.2.2</version>
                <executions>
                    <execution>
                        <id>build-image</id>
                        <phase>package</phase>
                        <goals>
                            <goal>build</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <imageName>${docker.registryUrl}/${docker.namespace}/${project.artifactId}</imageName>
                    <forceTags>true</forceTags><!-- 每次新的构建上覆盖镜像tags -->
                    <imageTags>
                        <imageTag>${docker.imageTag}</imageTag> <!-- -DpushImageTags没有-DdockerImageTags=xxx时,默认推送这个版本 -->
                    </imageTags>
                    <baseImage>openjdk:8-jdk-alpine</baseImage>
                    <entryPoint>["sh", "-c",  "java $JAVA_OPTIONS -jar /${project.build.finalName}.jar"]</entryPoint>
                    <resources>
                       <resource>
                            <targetPath>/skywalking_agent</targetPath>
                            <directory>${skywalking_agent_dir}</directory>
                            <include>**/**</include>
                        </resource>
                    </resources>
                </configuration>
            </plugin>
~~~
##### node
~~~
安装yarn && 设置依赖源
npm config set registry "https://registry.npm.taobao.org" &\
npm install -g yarn &\
yarn config set registry "https://registry.npm.taobao.org" &\
yarn config set sass_binary_site "https://npm.taobao.org/mirrors/node-sass/" &\
yarn config set phantomjs_cdnurl "http://cnpmjs.org/downloads" &\
yarn config set electron_mirror "https://npm.taobao.org/mirrors/electron/" &\
yarn config set sqlite3_binary_host_mirror "https://foxgis.oss-cn-shanghai.aliyuncs.com/" &\
yarn config set profiler_binary_host_mirror "https://npm.taobao.org/mirrors/node-inspector/" &\
yarn config set chromedriver_cdnurl "https://cdn.npm.taobao.org/dist/chromedriver" &
~~~
##### 重启jenkins
~~~
#关掉占用9999端口的程序 
kill -9 $(lsof -i:9999|tail -1|awk '"$1"!=""{print $2}')
#jenkins启动 运行之后关掉shell窗口
export JENKINS_HOME=/usr/local/jenkins_home && nohup java -jar -Xms1024m -Xmx3072m -XX:PermSize=256m -XX:MaxPermSize=512m -Dhudson.util.ProcessTree.disable=true -Dpermissive-script-security.enabled=true -Dorg.apache.commons.jelly.tags.fmt.timeZone=Asia/Shanghai ${JENKINS_HOME}/jenkins.war --ajp13Port=-1 --httpPort=9999 --prefix="/jenkins2"   >/dev/null 2>&1 &
~~~
#### 备注
存在一些问题,一些方法调用的jenkins rest api,应该有内部的类或者方法的 望大佬指导下
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
