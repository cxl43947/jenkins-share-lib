//全局环境变量
env=[
        'build_node_command':"npm",
        "backup_dir":"/opt/backup",    //全局备份地址
        //maven 选项
        "build_maven_options":"-Dskywalking_agent_dir=/usr/local/sh/skywalking_agent",
        "dockerNameSpace":"test",  //docker命名空间
        "dockerRegistryUrl_pull":"registry.cn-hangzhou.aliyuncs.com",   //docker拉取地址
        "dockerRegistryUrl_push":"registry.cn-hangzhou.aliyuncs.com",  //docker推送地址
        //钉钉推送地址
        "dingding_webhook":"https://oapi.dingtalk.com/robot/send?access_token=钉钉机器人token",
        //==========================docker-compose 模板需要
        //nacos服务配置"
        'NACOS_DISCOVERY_NAMESPACE':"dev-0000-0000-0000-0000",
        'NACOS_CONFIG_NAMESPACE':"dev-0000-0000-0000-0000",
        'NACOS_DISCOVERY_ADDR':"192.168.0.202:8848",
        'NACOS_CONFIG_ADDR':"192.168.0.202:8848",
        'NACOS_CONFIG_GROUP':"supermanex",
        'NACOS_DISCOVERY_GROUP':"supermanex",
        'PROFILES_ACTIVE':"dev",
        //seata服务配置"
        'SEATA_SERVER_ADDR':"192.168.0.202:8091",
        //skywalking配置"
        'SW_AGENT_COLLECTOR_BACKEND_SERVICES':"192.168.0.202:11800"
]
//maven基础包配置
proMap = [
        'jeecg':"org.jeecgframework.boot:jeecg-boot-module-system",
]
//数据库相关配置
mysql_info=[
        "dbtest":["192.168.0.202","3307","root","root"]
]
//redis相关配置
redis_info=[
        "redis_2":["192.168.0.202","root",["/data/local/redis/6379/data/appendonly.aof","/data/local/redis/6379/data/dump.rdb"]],
]
//nginx相关配置
nginx_info=[
        "nginx_7":["192.168.0.202","root",["/usr/local/nginx/conf/nginx.conf"]]
]
