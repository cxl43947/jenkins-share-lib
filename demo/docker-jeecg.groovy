#!groovy
@Library('jenkins_util') _                     //引入共享库
def util=new com.util.Util()                     //共享库中的类

pipeline {
    agent any
    options {      // 参考 https://blog.csdn.net/qq351469076/article/details/80138342
        buildDiscarder(logRotator(numToKeepStr: '50'))  //为最近的管道运行的特定数量保存artifacts和控制台输出
        timestamps()    //准备由流水线生成的所有控制台输出，并在该行发出的时间运行
        checkoutToSubdirectory('.jenkins')   //在工作空间的子目录中执行自动源代码控制签出  如果不设置这个 后面代码再检出到工作目录前会被clean
        disableConcurrentBuilds() //不允许同时执行管道。可以用于防止同时访问共享资源等
        timeout(time:1,unit:'HOURS') //设置管道运行的超时时间，在此之后，Jenkins将中止管道
        //skipDefaultCheckout()  //在代理指令中，跳过从源代码控制中检出代码
    }
    triggers {
         GenericTrigger( //Generic Webhook Trigger webhook插件 url触发:generic-webhook-trigger/invoke?token= 参考https://www.jianshu.com/p/7873d2f0dd3e
             genericVariables: [
                     [key: 'webhook_user_name', value: '$.user_name',defaultValue:''],
                     [key: 'webhook_hook_name', value: '$.hook_name',defaultValue:'']
             ],
             token: "${env.JOB_BASE_NAME}" ,
             causeString: "${env.webhook_hook_name} by ${env.webhook_user_name}" ,
             printContributedVariables: true,
             printPostContent: true
         )
    }
    parameters {//定义参数
        string(name: 'scmStr', defaultValue: 'svn,svn://xxx/java/jeecg,cxl_svn, , ', description: '仓库信息[代码库类型,代码路径,分支名称,jenkins凭证,用户名]')
        extendedChoice  name: 'projects',value: 'jeecg',defaultValue: 'jeecg',  description: '项目',  multiSelectDelimiter: ',', quoteValue: false, saveJSONParameterToFile: false, type: 'PT_CHECKBOX'
        string(name: 'Version', defaultValue: '', description: '版本号 不填默认为SCM_VERSION')
        choice choices: ['192.168.0.202'], description: '部署机器ip', name: 'remote_ip'
        extendedChoice  name: 'Status',value: '打包,推送镜像,下载镜像,运行',defaultValue: '打包',  description: '构建流程', multiSelectDelimiter: ',', quoteValue: false, saveJSONParameterToFile: false, type: 'PT_CHECKBOX',  visibleItemCount: 4
        string(name: 'opt_commit', defaultValue: '', description: '构建备注')
    }
    /*environment {
    }*/
    stages {
        stage('更新代码'){
            steps{
                script{
                    util.load_config()  //加载配置
                    //拉取代码
                    scm=scmStr.split(',')
                    scmObj=util.scm_checkout(scm[0],scm[1],scm[2],scm[3],scm[4])
                }
            }
        }
        stage('打包'){
            when {expression { Status.contains('打包') }}
            steps{
                script{
                    util.build_maven()
                }
            }
        }
        stage('推送镜像'){
            when {expression { Status.contains('推送镜像') }}
            steps{
                script{
                    util.docker_push()
                }
            }
        }
        stage('下载镜像'){
            when {expression { Status.contains('下载镜像') }}
            steps{
                script{
                    util.docker_pull(remote_ip)
                }
            }
        }
        stage('运行'){
            when {expression { Status.contains('运行') }}
            steps {
                script{
                    util.docker_run(remote_ip,['NACOS_CONFIG_GROUP':"jeecg",'JAVA_OPTIONS':"-Xmx1024m -Xms1024m -Xmn256m"])
                }
            }
        }
    }
    post {
        always {
            script{
                util.post_always()
            }
        }
    }
}