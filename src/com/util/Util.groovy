package com.util

import groovy.json.JsonOutput

import groovy.json.JsonSlurper
import groovy.json.JsonSlurperClassic
import groovy.json.JsonBuilder
import hudson.model.*;
import groovy.transform.Field;
import groovy.util.*
import java.util.*;
import java.text.DateFormat;
//git@gitee.com:cxl43947/jenkins-scrpit.git
//class Util {
/*  public Util() {
      println "进入构造方法"
      load_config()
  }*/
/**
 ==================================================

 jenkins  pipeline-script 工具类

 ==================================================
 **/
//重写print方法

void println(a){
    echo "-------------------------------------------------------------"+a
}

void print(a){
    echo "-------------------------------------------------------------"+a
}
//定义一些全局配置
@Field def config=[
        env:[:],
        proMap:[:],
        mysql_info:[:],
        redis_info:[:],
        nginx_info:[:]
]
@Field def scmObj=[:]
//WORKSPACE脚本目录 去掉轻量级检出 会生成${JOB_NAME}@script文件夹
/**
 * 加载自定义配置       //暂时在外部调用   groovy构造方法貌似没效果
 * @param groovy_config_path  ${env.WORKSPACE}/.jenkins说明 外面options配置了checkoutToSubdirectory('.jenkins')
 * @return
 */

def load_config(groovy_config_path=(env.groovy_config_path?groovy_config_path:"${env.WORKSPACE}/.jenkins/resources/jenkins_config-"+(env.HUDSON_URL.contains('192.168.0.201')?'test':'prod')+".groovy")){
    println("初始化配置${groovy_config_path}")
    try{
        sh "pwd"
        sh "cat ${groovy_config_path}"
        if(groovy_config_path){
            def c= new ConfigSlurper().parse(new File(groovy_config_path).toURI().toURL())
            for(e in c){
                config.put(e.key,e.value)
            }
            println("config="+config)
            if(config.env){ //配置自定义的全局环境变量
                println "配置自定义的全局环境变量${config.env}"
                for(e in config.env){
                    if(!env["${e.key}"]){ //如果环境变量不存在 就赋值
                        env["${e.key}"]=e.value
                    }
                }
            }
        }
    }catch(Exception e){
        e.printStackTrace();
        e.print("配置读取失败"+e)
    }
}
/**
 * 获得远程环境变量
 * @param env
 * @return
 */
def getRemoteEnv(env=[:]){
    println "生成ssh环境变量 initRemoteEnv"
    remote_env=[
            //镜像版本号
            'Version':"${Version}",
            //实例ip 微服务寻址会用到
            'BASE_IP':"${remote_ip}",
            //java扩展参数
            'JAVA_OPTIONS':" "
    ]
    //覆盖原有变量
    remote_env.putAll(config.remote_env)
    remote_env.putAll(env)
    return remote_env
}
//代码拉取脚本 返回值是所拉取的代码信息 svn指定版本号(${SCM_URL}@版本号) git指定版本号(SCM_BRANCH设置成版本号即可)
def scm_checkout(SCM_TYPE='git',SCM_URL,SCM_AUTH_PASS=' ',SCM_BRANCH=' ',SCM_AUTH_USER=' '){
    SCM_BRANCH=SCM_BRANCH&&SCM_BRANCH.trim()!=''?SCM_BRANCH:'origin/master'
    println "开始检出${SCM_URL}---[${SCM_BRANCH}]"
    sh "pwd"
    // scmObj = [:]
    if (SCM_TYPE =='git') {
        scmObj=checkout([$class: "GitSCM", branches: [[name: "${SCM_BRANCH}"]], \
                         doGenerateSubmoduleConfigurations: false, extensions: [], \
                          submoduleCfg: [], userRemoteConfigs: [[credentialsId: "${SCM_AUTH_PASS}", \
                          url: "${SCM_URL}"]]])
        scmObj.SCM_VERSION=scmObj.GIT_COMMIT.substring(0,7)
    }else if (SCM_TYPE == 'tfs' ) {
        // need SCM_AUTH_USER args
        scmObj= checkout(changelog: false, poll: false, scm: [$class: 'TeamFoundationServerScm', \
                         password: new hudson.util.Secret("$SCM_AUTH_PASS"), projectPath: "${SCM_BRANCH}", \
                         serverUrl: "${SCM_URL}", useOverwrite: true, useUpdate: true, userName: "$SCM_AUTH_USER", \
                         workspaceName: 'Hudson-${JOB_NAME}'])
    }else if (SCM_TYPE == 'svn' ){
        scmObj= checkout([$class: 'SubversionSCM', additionalCredentials: [], excludedCommitMessages: '', excludedRegions: '', \
                  excludedRevprop: '', excludedUsers: '', filterChangelog: false, ignoreDirPropChanges: false, includedRegions: '', \
                  locations: [[cancelProcessOnExternalsFail: true, credentialsId: "${SCM_AUTH_PASS}", \
                               depthOption: 'infinity', ignoreExternalsOption: true, local: '.', \
                               remote: "${SCM_URL}"]], \
                  quietOperation: true, workspaceUpdater: [$class: 'UpdateUpdater']])
        //workspaceUpdater 类型CheckoutUpdater(清除再更新)  UpdateUpdater(始终使用更新)
        scmObj.SCM_VERSION=scmObj.SVN_REVISION
    }
    scmObj.SCM_URL=SCM_URL
    scmObj.SCM_TYPE=SCM_TYPE
    env.scmObj=scmObj
    if(!env.Version){
        println "没有输入Version 设置Version="+scmObj.SCM_VERSION
        env.Version=scmObj.SCM_VERSION
    }else{
        println "已设置Version="+env.Version
    }
    println env.scmObj;
    return scmObj;
}

//============================================================== maven  ==============================================================
//mvn打包 同时构建镜像
def build_maven(
        WORKSPACE=env.WORKSPACE,
        pomFile="pom.xml",
        build_maven_java_options=env.build_maven_options,
        projects=env.projects,
        dockerRegistryUrl="${env.dockerRegistryUrl_push}",dockerNameSpace="${env.dockerNameSpace}",dockerImageTags="${env.Version}"){
    def projectsArr=projects.split(',')
    def packages=[]
    for (e in projectsArr ) { //动态选择需要打包的项目
        if(config.proMap[e]){ packages.add(config.proMap[e])}
    }
    def packagesStr=packages.join(",")
    //println "cd ${WORKSPACE} && mvn clean package -f ${pomFile} ${build_maven_java_options} -Ddocker.imageTag=${dockerImageTags}  -Ddocker.registryUrl=${dockerRegistryUrl} -Ddocker.namespace=${dockerNameSpace}  -pl ${packagesStr}  -am"
    //-DpushImageTag -DdockerImageTags=${dockerImageTags} 这里分开镜像打包和推送   动态设置maven docker插件配置了变量
    sh "cd ${WORKSPACE} && mvn clean package -f ${pomFile} ${build_maven_java_options} -Ddocker.imageTag=${dockerImageTags}  -Ddocker.registryUrl=${dockerRegistryUrl} -Ddocker.namespace=${dockerNameSpace}  -pl ${packagesStr}  -am"
    //maven clean
    sh "cd ${WORKSPACE} && mvn clean -f ${pomFile}  -pl ${packagesStr}  -am"
}
//============================================================== docker  ==============================================================
//推送镜像
def docker_push(dockerRegistryUrl="${env.dockerRegistryUrl_push}",dockerNameSpace="${env.dockerNameSpace}",dockerImageTags="${env.Version}",
                projects=env.projects){
    println "开始镜像推送"
    def projectsArr=projects.split(',')
    def shStr="";
    for (e in projectsArr) {
        shStr="\tdocker push ${dockerRegistryUrl}/${dockerNameSpace}/${e}:${Version}\n"
        println shStr
        sh shStr
    }
}
//拉取镜像 直接从公网拉
/**
 *
 * @param remote_ip
 * @param remote_user
 * @param dockerRegistryUrl
 * @param dockerNameSpace
 * @param dockerImageTags
 * @param projects
 * @return
 */
def docker_pull(remote_ip=env.remote_ip,remote_user=(env.remote_user?env.remote_user:"root"),
                dockerRegistryUrl="${env.dockerRegistryUrl_pull}",dockerNameSpace="${env.dockerNameSpace}",dockerImageTags="${env.Version}",
                projects=env.projects
){
    println "开始镜像拉取"
    def projectsArr=projects.split(',')
    def shStr="ssh -t $remote_user@$remote_ip << remotessh\n";
    for (e in projectsArr) {
        shStr+="\tdocker pull ${dockerRegistryUrl}/${dockerNameSpace}/${e}:${Version}\n"
    }
    shStr+="remotessh"
    print shStr
    sh shStr
}
/**
 * 运行docker   直接在远程运行模板
 * @param run_type  运行方式1(docker-compose) 2(docker stack deploy)
 * @param docker_compose_dir docker-compose文件目录
 * @param remote_env    远程使用的环境变量
 * @param remote_ip     远程主机
 * @param remote_user     远程用户
 * @param dockerRegistryUrl
 * @param dockerNameSpace
 * @param dockerImageTags
 * @return
 */
def docker_run2(remote_ip=env.remote_ip,
                remote_env,
                run_type=1,
                docker_compose_dir=(env.docker_compose_dir?env.docker_compose_dir:"${env.WORKSPACE}/.jenkins/docker-compose/"),
                remote_user=(env.remote_user?env.remote_user:"root"),
                dockerRegistryUrl="${env.dockerRegistryUrl_pull}",dockerNameSpace="${env.dockerNameSpace}",dockerImageTags="${env.Version}"){
    println "复制docker-compose文件到远程主机"
    sh "ssh $remote_user@$remote_ip 'mkdir -p $docker_compose_dir'"
    sh "scp -r $docker_compose_dir $remote_user@$remote_ip:$docker_compose_dir/../"
    def shStr="ssh -t $remote_user@$remote_ip << remotessh\n";
    shStr+="\tcd ${docker_compose_dir}\n"
    println "设置环境变量"
    for (e in remote_env) {if(e.value){shStr+="\texport ${e.key}='${e.value}' \n"}}
    println "开始启动docker容器"
    def projectsArr=projects.split(',')
    for (e in projectsArr) {
        //输出模板
        shStr+="echo '------------------------------------------------------------' && envsubst < docker-${e}.yml && echo -e '\n------------------------------------------------------------'\n"
        if(run_type==1){ //docker-compose
            shStr+="\tdocker-compose -f docker-${e}.yml stop && docker-compose -f docker-${e}.yml up --build -d \n"
        }else{  //docker stack
            shStr+="\tdocker stack deploy --compose-file=docker-${e}.yml  ${e} \n"
        }
    }
    shStr+="\nremotessh"
    print shStr
    sh shStr
}
//生成文件 再复制到远程运行
def docker_run(remote_ip=env.remote_ip,
               run_env=[:],
               run_type=1,
               docker_compose_dir=(env.docker_compose_dir?env.docker_compose_dir:"${env.WORKSPACE}/.jenkins/docker-compose/"),
               remote_user=(env.remote_user?env.remote_user:"root"),
               backup_dir=(env.backup_dir?env.backup_dir:"/usr/local/sh/backup/")+"/docker-compose/${env.JOB_NAME}/${env.Version}",
               dockerRegistryUrl="${env.dockerRegistryUrl_pull}",dockerNameSpace="${env.dockerNameSpace}",dockerImageTags="${env.Version}"){
    //========================================================================= 模板
    println "根据模板生成docker-compose文件"
    def shStr="";
    for (e in run_env) {    //覆盖原有变量
        env["${e.key}"]="${e.value}"
    }
    def projectsArr=projects.split(',')
    remote_deploy_dir="/opt/docker-compose"
    shStr= "\nssh $remote_user@$remote_ip 'mkdir -p ${remote_deploy_dir}'\n"
    shStr+="mkdir -p ${backup_dir}\n"
    for (e in projectsArr) {
        newFile="${backup_dir}/docker-${e}.yml"
        shStr+= "envsubst < ${docker_compose_dir}/docker-${e}.yml > ${newFile}\n"  //模板赋值生成新文件
        shStr+= "echo '------------------------------------------------------------' && cat ${newFile} && echo -e '\n------------------------------------------------------------'\n"
        shStr+= "scp -r ${newFile} $remote_user@$remote_ip:$remote_deploy_dir/\n"  //复制新的文件到远程目标
    }
    print shStr
    sh shStr
    //========================================================================= 远程运行
    println "${remote_ip}运行docker-compose"
    shStr+="\nssh -t $remote_user@$remote_ip << remotessh\n";
    shStr+="\tcd ${remote_deploy_dir}\n"
    println "开始启动docker容器"
    for (e in projectsArr) {
        //输出模板
        if(run_type==1){ //docker-compose
            shStr+="\tdocker-compose -f docker-${e}.yml stop && docker-compose -f docker-${e}.yml up --build -d \n"
        }else{  //docker stack
            shStr+="\tdocker stack deploy --compose-file=docker-${e}.yml  ${e} \n"
        }
    }
    shStr+="remotessh"
    print shStr
    sh shStr
}
//============================================================== node  ==============================================================
//node打包
def build_node(dist_dir=env.dist_dir?env.dist_dir:"${env.WORKSPACE}/dist/",
               command=(env.build_node_command?env.build_node_command:"yarn"),  //npm 或 yarn
               backup_dir=(env.backup_dir?env.backup_dir:"/usr/local/sh/backup/")+"/nginx/${env.JOB_NAME}",
               web_release_dir=env.web_release_dir?env.web_release_dir:"/usr/local/sh/release/${JOB_NAME}/${env.Version}",
               deploy_file=env.Version+".tar.gz"){
    println "build_node"
    sh "mkdir -p $backup_dir"
    sh "${command} install --unsafe-perm"
    sh "${command} run build"
    println "开始压缩和备份"
    sh "mkdir -p ${web_release_dir} && \\cp -r ${dist_dir}/*  ${web_release_dir}"
    //sh "cd ${dist_dir} && tar -zcvf ${deploy_file} ./** --remove-files && \\mv -f ${deploy_file} ${backup_dir}" //打包时删除源文件
    sh "cd ${dist_dir} && tar -zcvf ${deploy_file} ./** && \\mv -f ${deploy_file} ${backup_dir}"
    delOldFileByFileDir(backup_dir)
}

//============================================================== nginx ==============================================================
//复制打包后的网页文件到nginx部署目录
def nginx_deploy(remote_deploy_dir=env.remote_deploy_dir,remote_ip=env.remote_ip,remote_user=(env.remote_user?env.remote_user:"root"),
                 backup_dir=(env.backup_dir?env.backup_dir:"/usr/local/sh/backup/")+"/nginx/${env.JOB_NAME}",
                 deploy_file=env.Version+".tar.gz"){
    println "nginx_deploy"
    sh "ssh $remote_user@$remote_ip 'mkdir -p ${remote_deploy_dir}'"
    sh "scp -r ${backup_dir}/${deploy_file}  $remote_user@\$remote_ip:${remote_deploy_dir}"
    sh "ssh $remote_user@$remote_ip 'cd ${remote_deploy_dir} && tar -xzvf ${deploy_file} && rm -rf ${deploy_file}'"
}
//重新加载配置文件
def nginx_reload(remote_ip=env.remote_ip,remote_user=(env.remote_user?env.remote_user:"root"),nginx_bin_dir=''){
    println "nginx_reload"
    if(nginx_bin_dir){
        sh "ssh $remote_user@$remote_ip 'cd $nginx_bin_dir && ./nginx -s reload'"
    }else{
        sh "ssh $remote_user@$remote_ip 'nginx -s reload'"
    }
}

@NonCPS //该方法内部的CPS转换。可以这样做来解决Groovy语言覆盖范围的局限性（因为该方法的主体将使用本机Groovy语义执行），或获得更好的性能（解释器会带来相当大的开销）
//防止sort方法被转换
def fileSort(List fileList){//按修改时间倒序
    return fileList.sort {a,b -> return b.lastModified()-a.lastModified() }
}
/**
 * 删除指定目录下的旧文件
 * @param fileDir   文件目录
 * @param saveCount   最近文件保留个数
 * @param layer     验证文件夹层级
 * @return
 */
def delOldFileByFileDir(fileDir,saveCount=100,layer=4){
    if(!fileDir.matches("(/.+){"+layer+"}")){
        println "${fileDir}层级小于4,停止删除"
        return;
    }
    println "删除${fileDir}下的旧文件,保留${saveCount}个"
    File file = new File(fileDir)
    if(file.isFile()){
        println "删除目标不是文件夹,停止删除"
        return "删除目标不是文件夹"
    }
    //获得文件列表
    List fileList = Arrays.asList(file.listFiles())
    //按修改时间倒序
    fileList=fileSort(fileList)
    println fileList.getClass().toString()
    println "fileList="+fileList
    //删除文件
    for (int i = saveCount; i < fileList.size() ; i++) {
        /*if(saveCount<i) {
            println "删除"+"-"+fileList[i].lastModified()+"\t"+new Date(fileList[i].lastModified()).format("yyyy-MM-dd HH:mm:ss.SSS")+"\t"+fileList[i].getName()
        } else {
            println "保留"+"-"+fileList[i].lastModified()+"\t"+new Date(fileList[i].lastModified()).format("yyyy-MM-dd HH:mm:ss.SSS")+"\t"+fileList[i].getName()
        }*/
        println "删除文件:"+fileList[i].getName()+"\t删除状态:"+fileList[i].delete()
    }
}
//============================================================== 备份相关 ==============================================================
//获取指定时间段增量sql
def getBinLogSql(startDate,
                 stopDate,
                 log_name='/var/log/mysql/mysql-bin.0*',
                 db_name='supermanex',
                 remote_ip="",emote_user=env.remote_user,
                 db_host="",db_port=3306,db_user="root",db_password="root",
                 backupPath=(env.backup_dir?env.backup_dir:"/usr/local/sh/backup/")+"/mysql/${remote_ip}/${db_name}_binlogSql/"
){
    if(!startDate||!startDate){
        return "开始时间和结束时间不能为空";
    }
    println "----------------------------------开始获取${startDate}-${stopDate}增量sql"
    backupFile="${db_name}_${startDate}__${stopDate}.sql.gz".replaceAll("( |-|:)","")
    def str="";
    //log_name='/var/lib/mysql/mysql-bin.00000*'
    if(remote_ip){
        str+="ssh -t $remote_user@$remote_ip << remotessh\n";
    }
    str+="\tmkdir -p ${backupPath}\n"
    str+="\tmysqlbinlog  --no-defaults -d${db_name} -vv -s --base64-output=decode-rows \\\n"
    if(db_host){//去远程服务器执行binlog命令,读取远程服务器本地文件  文件名不支持通配符
        str+="\t--read-from-remote-server -h${db_host} -P${db_port} -u${db_user} -p${db_password} -d${db_name} \\\n"
    }
    str+="\t--start-datetime='${startDate}' \\\n"
    str+="\t--stop-datetime='${stopDate}' \\\n"
    str+="\t${log_name} \\\n"
    str+="\t| gzip > ${backupPath}/${backupFile}  \n"
    if(remote_ip){
        str+="remotessh \n"
    }
    str+="echo '下载文件${backupPath}/${backupFile}到本机' \nscp ${remote_user}@${remote_ip}:${backupPath}/${backupFile} ${backupPath}"
    println str;
    //return ["backupPath":backupPath,"backupFile":backupFile]
    return "${backupPath}/${backupFile}"
}
//mysql备份
def dump_mysql(db_host="192.168.0.202",db_port=3307,db_user="root",db_password="root",db_name='test',
               backupPath=(env.backup_dir?env.backup_dir:"/usr/local/sh/backup/")+"/mysql2/${db_host}/${db_name}/"
){
    println "备份mysql\t${db_name}"
    sh "mkdir -p ${backupPath}"
    def backupFile="${db_name}_"+new Date().format("yyyyMMddHHmmss")+".sql.gz"
    def shStr="mysqldump -h${db_host} -P${db_port} -u${db_user} -p${db_password} ${db_name} | gzip > ${backupPath}/${backupFile}"
    println shStr
    sh shStr
    delOldFileByFileDir(backupPath,30)
}
//把备份文件构建成镜像并推送到远程镜像仓库
def push_backup(imgTag="registry.cn-hangzhou.aliyuncs.com/wd-supermanex/backup:test"){
    sh "docker build -f /usr/local/sh/docker/Dockerfile -t ${imgTag} .  && docker push ${imgTag} && rm-rf /usr/local/sh/docker/backup/*"
}
//创建数据库并导入数据 全量备份和增量备份
/**
 *
 * @param db_name
 * @param stopDate
 * @param backupPath
 * @param db_host
 * @param db_port
 * @param db_user
 * @param db_password
 * @return
 */
def mysqlImport(db_name,stopDate,backupPath,
                db_host="192.168.0.202",db_port=3306,db_user="root",db_password="root"){
    println "开始创建数据库和还原"
    startDate=stopDate.substring(0,11)+"00:52:00"
    def new_db_name="${db_name}_"+startDate.replaceAll("( |-|:)","")
    file1=new_db_name+".sql"
    def s1="";
    s1+="gunzip -c ${file1}.gz > ${file1} \n"
    println startDate
    file1="${backupPath}/${db_name}_202011050052.sql"
    println "创建数据库${db_name}"
    s1+="mysql -h${db_host} -P${db_port} -u${db_user} -p${db_password} --max_allowed_packet=500M -e 'drop database if exists `${new_db_name}`;create database `${new_db_name}` DEFAULT CHARACTER SET utf8;'\n"
    println "全量恢复${file1}"
    s1+="mysql -h${db_host} -P${db_port} -u${db_user} -p${db_password} --max_allowed_packet=500M --default-character-set=utf8 ${new_db_name} < ${file1} \n"
    file2=getBinLogSql(startDate,stopDate,"/var/log/mysql/mysql-bin.0*",db_name)
    file2=file2.replace(".gz","")
    s1+="gunzip -c ${file2}.gz > ${file2}\n"
    println "增量恢复${file2}"
    s1+="mysql -h${db_host} -P${db_port} -u${db_user} -p${db_password} --max_allowed_packet=500M --default-character-set=utf8 ${new_db_name} < ${file2} \n"
    println "清除解压的sql文件"
    s1+="rm -f ${file1} \nrm -f ${file2}"
    println s1
}
//redis备份
def dump_redis(remote_host="192.168.0.202",remote_user=(env.remote_user?env.remote_user:"root"),remote_rdb="",remote_aof="",
               backupPathBase="/usr/local/sh/backup/redis/${remote_host}/"){
    println "备份redis\t${remote_host}"
    backupPath="${backupPathBase}"+new Date().format("yyyyMMddHHmmss")
    shStr="mkdir -p ${backupPath}\n";
    if(remote_rdb){
        shStr+="scp ${remote_user}@${remote_host}:${remote_rdb} ${backupPath}\n"
    }
    if(remote_aof){
        shStr+="scp ${remote_user}@${remote_host}:${remote_aof} ${backupPath}\n"
    }
    println shStr
    sh shStr
    delOldFileByFileDir(backupPathBase,480)
}

//定义构建完成之后的操作
def post_always(otherMessage2=[:],
                dingding_webhook=(env.dingding_webhook?env.dingding_webhook:""),
                title="Jenkins构建通知"){
    /* try{*/
    def json = getCurBuild(otherMessage2);
    env.BUILD_USER=json.BUILD_USER;
    env.BLUE_BUILD_URL=json.BLUE_BUILD_URL;
    env.BUILD_TIME=json.BUILD_TIME;
    try{
        env.pre_run_number=json.preRun.BUILD_DISPLAY_NAME
        env.pre_BUILD_URL=json.preRun.BUILD_URL
        env.pre_execute_version=json.preRun.execute_version
    }catch(Exception e){
        e.printStackTrace()
        println("获取上次构建信息失败")
    }
    saveRecord(json)
    sendDingDingWebhook(otherMessage2,dingding_webhook,title)
    /*}catch(Exception e){
        println "post_always执行失败"
        e.printStackTrace()
    }*/

    /*json.putAll(params)
    json.putAll(otherMessage2)
    if(scmObj&&scmObj.size()>0){
        json.put("scm_version",scmObj.SCM_VERSION)
        json.put("execute_version",env.Version)
    }
    json.putAt("BUILD_TIME",env.BUILD_TIME)
    json.putAt("BUILD_STATUS",env.BUILD_STATUS)
    json.putAt("BUILD_USER",env.BUILD_USER)
    json.putAt("blue_BUILD_URL",env.blue_BUILD_URL)
    json.putAt("BUILD_URL",env.BUILD_URL)
    json.putAt("JOB_NAME",env.JOB_NAME)*/


}

//保存构建记录
def saveRecord(json = [:],es_addr=(env.es_addr?env.es_addr:"http://192.168.0.202:9200/jenkins_build_log")){
    try{
        def jsonStr=JsonOutput.toJson(json);
        println("json=${jsonStr}")
        sh "curl -H 'Content-Type: application/json' -XPOST ${es_addr}/jenkins_build_log -d '${jsonStr}'"
    }catch(Exception e){
        println("保存构建记录失败")
    }
}

//钉钉通知
def sendDingDingWebhook(otherMessage2=[:],dingding_webhook=(env.dingding_webhook?env.dingding_webhook:""),
                        title="Jenkins构建通知"){
    def resultMap=[
            "SUCCESS":"<font color=#52c41a>成功</font>",
            "FAILURE":"<font color=#f5222d>失败</font>",
            "ABORTED":"<font color=#13c2c2>取消</font> ",
    ]
    BUILD_STATUS=resultMap.get(currentBuild.currentResult)
    //构建状态
    env.BUILD_STATUS=BUILD_STATUS?BUILD_STATUS:currentBuild.currentResult
    if(currentBuild.currentResult=="ABORTED"){
        println("构建被终止 不发送消息")
        return;
    }
    try{
        println "发送钉钉消息" //<font color=#f5222d>失败</font>  <font color=#13c2c2>取消</font> <font color=#52c41a>成功</font>
        /*def text="# [${env.JOB_NAME}](${env.JOB_URL})\\n---\\n" +
                "- 任务：[${env.BUILD_DISPLAY_NAME}](${env.BUILD_URL}) -- [blue_view](${env.blue_BUILD_URL}) \\n" +
                "- 状态：${env.BUILD_STATUS} \\n" +
                "- 持续时间：${env.BUILD_TIME} \\n" +
                "- 执行人：${env.BUILD_USER} \\n"*/
        def otherMessage=[:]
        def text="**[${env.JOB_NAME}](${env.BUILD_URL})** [${env.BUILD_DISPLAY_NAME}](${env.BLUE_BUILD_URL})\\n" +
                "- ${env.BUILD_STATUS} - ${env.BUILD_TIME} ";
        if(env.pre_execute_version){
            text+=" - [pre_${env.pre_run_number}](${env.pre_BUILD_URL})"
        }
        text+="\\n- 执行人：${env.BUILD_USER} \\n"
        otherMessage.putAll(params)
        otherMessage.putAll(otherMessage2)
        //println "scmObj--${scmObj}"
        def desc="执行人：${env.BUILD_USER}<br/>构建时间：${env.BUILD_TIME}";
        if(scmObj&&scmObj.size()>0){
            text+="- 版本：<font color=#520cfc>${env.Version?env.Version:''}</font> " +
                    "<font color=#0775d5>${scmObj.SCM_VERSION?scmObj.SCM_VERSION:''}</font>" +
                    " <font color=#009688>${env.pre_execute_version?env.pre_execute_version:''}</font>\\n"
            //设置job描述
            desc+="<br/>stage:${otherMessage.stage}<br/>版本：<span style='color:#520cfc'>${env.Version?env.Version:''}</span>" +
                    "&nbsp;<span style='color:#0775d5'>${scmObj.SCM_VERSION?scmObj.SCM_VERSION:''}</span>" +
                    "&nbsp;<span style='color:#009688'>${env.pre_execute_version?env.pre_execute_version:''}</span>"
            desc=desc.replace('运行','<span style="color:#bb9b02;font-weight:bold">运行</span>')
            println("desc="+desc)
            otherMessage.stage=otherMessage.stage.replace("运行","<font color=#bb9b02>运行</font>")
        }
        def otherMessageStr="";
        for (e in otherMessage){
            if(e.value){
                if(!e.key.matches(/.*(remote_ip|scmStr|remote_deploy_dir).*/)){ //跳过部分信息
                    otherMessageStr+="- ${e.key}：${e.value}\\n"
                }
                if(!e.key.matches(/.*(stage).*/)){
                    desc+="<br/>${e.key}：${e.value}"
                }
            }
        }
        text+=otherMessageStr;
        //设置描述
        sh "curl  -X POST --data-urlencode 'description=${desc}' "+BUILD_URL.replace("http://","http://${env.jenkinsToken}@")+"/submitDescription"
        //发送钉钉消息
        def commod="curl -H 'Content-Type:application/json' -X POST -d \\\n\
                 '{\n" +
                "    \"msgtype\": \"actionCard\",\n" +
                "    \"actionCard\": {\n" +
                "        \"title\": \"${title}\", \n" +
                "        \"text\": \"${text}\"" +
                /*"        ,\"btnOrientation\": \"1\" \n" +
                 "        ,\"btns\": [\n" +
                 "            {\"title\": \"更改记录\", \"actionURL\": \"https://www.dingtalk.com/\"}, \n" +
                 "            {\"title\": \"控制台\", \"actionURL\": \"https://www.dingtalk.com/\"}\n" +
                 "        ]\n" +*/
                "    }\n" +
                "}'  \\\n" +
                "${dingding_webhook}"
        sh commod
    }catch(Exception e){
        println "钉钉通知失败"
        e.printStackTrace()
    }
}
//获取构建信息
def getCurBuild(otherMessage2=[:]){
    def jsonSlurper = new JsonSlurperClassic()
    def jsonOutput = new JsonOutput()
    //调用jenkins远程api
    def apiResponse="$JENKINS_URL/job/$JOB_NAME/$BUILD_NUMBER/api/json".toURL().text
    // println(jsonOutput.prettyPrint(apiResponse))
    def apiMap = jsonSlurper.parseText(apiResponse)
    def causes0=[:]
    for(e in apiMap.actions){
        def _class=e.get("_class");
        if(_class){
            if(_class=="hudson.model.CauseAction"){
                causes0=e.causes[0]
                break;
            }
        }
    }
    //def causes0 = apiMap.actions[0].causes[0];
    def curBuild=[
            "JOB_NAME":env.JOB_NAME,        //任务名
            "BUILD_URL":env.BUILD_URL,
            "BUILD_DISPLAY_NAME":env.BUILD_DISPLAY_NAME,
            //构建原因
            "BUILD_USER":env.webhook_user_name?"${env.webhook_hook_name} by ${env.webhook_user_name}":causes0.userName?causes0.userName:causes0.shortDescription,       // 构建人
            "startTime":new Date(apiMap.getAt("timestamp")).format("yyyy-MM-dd HH:mm:ss.SSS"), //构建开始时间
            "endTime":Calendar.getInstance().format("yyyy-MM-dd HH:mm:ss.SSS"), //构建结束时间
            "BUILD_STATUS":currentBuild.currentResult,                  //构建状态
            "BLUE_BUILD_URL":"${JENKINS_URL}/blue/organizations/jenkins/${JOB_NAME}/detail/${JOB_NAME}/${BUILD_NUMBER}",                  //任务视图
            "BUILD_TIME":currentBuild.durationString.replace('and counting',''), //构建花费时间
    ]
    //======================================== 上次成功部署信息

    //es查询 任务完成时间倒序 http://192.168.0.202:9200/jenkins_build_log/_search?q=stage:%E8%BF%90%E8%A1%8C%20AND%20JOB_NAME:${env.JOB_NAME}%20AND%20BUILD_STATUS:SUCCESS&size=1&sort=endTime:desc
    try{
        def preRunResponse="${env.es_addr?env.es_addr:'http://192.168.0.202:9200'}/_search?_source=startTime,BUILD_URL,BUILD_DISPLAY_NAME,execute_version&q=stage:%E8%BF%90%E8%A1%8C%20AND%20JOB_NAME:${env.JOB_NAME}%20AND%20BUILD_STATUS:SUCCESS&size=1&sort=endTime:desc".toURL().text
        println(jsonOutput.prettyPrint(preRunResponse))
        //获取到的是Map对象
        def hits = jsonSlurper.parseText(preRunResponse).getAt("hits").getAt("hits")
        println "hits="+hits
        preRun=hits.getAt("_source")
        println("${preRun.endTime}----${preRun.execute_version}----${preRun.BUILD_URL}"+preRun.getAt("BUILD_URL"))
        curBuild.put("preRun" ,preRun[0])
    }catch(Exception e){
        println("获取上次构建记录失败")
    }
    //======================================== 自定义版本号
    if(scmObj&&scmObj.size()>0){
        curBuild.put("scm_version",scmObj.SCM_VERSION)
        curBuild.put("execute_version",env.Version)
    }
    curBuild.putAll(params)
    curBuild.putAll(otherMessage2)
    return curBuild;
}

//获得当前用户
def getCurrentUser(){
    if(env.BUILD_USER){ //如果环境变量存在
        return env.BUILD_USER;
    }else if(env.webhook_user_name){  //如果是webhook调用
        return "${env.webhook_hook_name} by ${env.webhook_user_name}";
    }else{
        def job = Jenkins.getInstance().getItemByFullName(env.JOB_BASE_NAME, Job.class)
        def build = job.getBuildByNumber(env.BUILD_ID as int)
        def userId = build.getCause(Cause.UserIdCause).getUserId()
        /*def user = User.current()
        println "登录用户ID is $userId" //返回当前登录用户ID “zhangsan”
        println "user is $user"  //返回“System”*/
        return userId
    }
    /*build user vars 插件调用
    wrap([$class: 'BuildUser']) {
        script { env.BUILD_USER = "${env.BUILD_USER}" }
    }*/
}
//}
